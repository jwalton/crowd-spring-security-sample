import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;


public class SimpleServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");

        PrintWriter w = resp.getWriter();

        if (req.getRequestURI().equals("/console/accessdenied.action"))
        {
            w.println(" XXXX Access Denied XXXX");
        }

        w.println(new Date().toString());
        w.println(this);

        w.println("Serving: " + req.getRequestURI());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null)
        {
            w.println("Auth is " + auth.getName());

            w.println("\nAuthorities:");
            for (GrantedAuthority ga : auth.getAuthorities())
            {
                w.println(" " + ga);
            }
        }
        else
        {
            w.println("(auth unavailable)");
        }
    }
}
