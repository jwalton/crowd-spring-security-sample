## Crowd Spring Security sample

An example of using [Spring Security](http://projects.spring.io/spring-security/) to authenticate to [Atlassian Crowd](https://www.atlassian.com/software/crowd).

### To configure

Edit `src/main/resources/crowd.properties` to point to a configured Crowd instance.

Ensure you get the `com.atlassian.crowd` artifacts from Atlassian's public Maven repository. To enable this in your Maven configuration, see the repository changes in
[Maven 2 Integration](https://developer.atlassian.com/display/CROWDDEV/Maven+2+Integration) for details.

### To run

1. Ensure Crowd is running
2. Start this example:

        mvn org.mortbay.jetty:jetty-maven-plugin:run -Djetty.port=9000

3. Visit [http://localhost:9000/](http://localhost:9000/).

### Development

See `src/main/webapp/WEB-INF/applicationContext-security.xml` for the wiring and security policy.

See [Integrating Crowd with Spring Security](https://confluence.atlassian.com/display/CROWD/Integrating+Crowd+with+Spring+Security) for further details.
